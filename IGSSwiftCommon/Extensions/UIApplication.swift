//
//  UIApplication.swift
//  UIControls
//
//  Created by Jonathan Hebert on 1/4/16.
//  Copyright © 2016 MassMutual. All rights reserved.
//

import UIKit

public extension UIApplication {
    @objc var topViewController: UIViewController? {
        guard let rootViewController = UIApplication.shared.keyWindow?.rootViewController else {
            //TODO: Log
            return nil
        }
        
        return topOfViewStack(forViewController: rootViewController)
    }
    
    fileprivate func topOfViewStack(forViewController viewController: UIViewController) -> UIViewController? {
        if let navigationController = viewController as? UINavigationController {
            guard let topViewController = navigationController.topViewController else {
                //TODO: Log
                return nil
            }

            return topOfViewStack(forViewController: topViewController)
        }
        
        if let tabBarController = viewController as? UITabBarController {
            guard let selectedTabViewController = tabBarController.selectedViewController else {
                //TODO: Log
                return nil
            }
            
            return topOfViewStack(forViewController: selectedTabViewController)
        }
        
        if let presentedViewController = viewController.presentedViewController {
            return topOfViewStack(forViewController: presentedViewController)
        }
        
        return viewController
    }
}
