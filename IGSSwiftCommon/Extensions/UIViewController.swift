//
//  UIViewController.swift
//  TeslaRemote
//
//  Created by Jonathan on 9/23/18.
//  Copyright © 2018 IronGeekSolutions. All rights reserved.
//

import UIKit
import MBProgressHUD

public extension UIViewController {
    
    func showHUD(text: String? = nil) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .annularDeterminate
        hud.label.text = text
        DispatchQueue.main.async {
            hud.show(animated: true)
        }
    }
    func dismissHUD() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }

    func showTwoButtonAlert(message: String, title: String? = nil, defaultText: String, defaultAction: (() -> Void)? = nil, cancelText: String, cancelAction: (() -> Void)? = nil, isCancelDestructive: Bool? = false) {
        
        let defaultHandler: (UIAlertAction) -> Void = { _ in
            defaultAction?()
        }
        let cancelHandler: (UIAlertAction) -> Void = { _ in
            cancelAction?()
        }
        let defaultAlertAction = UIAlertAction(title: defaultText,
                                               style: .default,
                                               handler: defaultHandler)
        let cancelAlertAction = UIAlertAction(title: cancelText,
                                              style: isCancelDestructive == true ? .destructive : .cancel,
                                              handler: cancelHandler)
        
        let alert = UIAlertController(title: title,
            message: message,
            preferredStyle: .alert)
        alert.addAction(defaultAlertAction)
        alert.addAction(cancelAlertAction)
        
        self.present(alert, animated: true)
    }
    
    func showAlert(message: String, buttonText: String, title: String? = nil, buttonAction: (() -> Void)? = nil) {
                
        let alertHandler: (UIAlertAction) -> Void = { _ in
            buttonAction?()
        }
        
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(title: buttonText,
                          style: .default,
                          handler: alertHandler)
        )
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func performSegue<S: RawRepresentable>(withIdentifier identifier: S, sender: Any?) where S.RawValue == String {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.performSegue(withIdentifier: identifier.rawValue, sender: sender)
        }
    }
}

public extension UIViewController {
    
    func loadingStart(_ color: UIColor = UIColor.lightGray) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let overlayView = OverlayView(frame: self.view.bounds)
            overlayView.backgroundColor = color
            overlayView.alpha = 0.8
            
            self.view.isUserInteractionEnabled = false
            self.view.addSubview(overlayView)
            Loader.show(inView: self.view)
        }
    }

    func loadingEnd() {
        DispatchQueue.main.async { [weak self] in
            Loader.dismiss()
            self?.view.isUserInteractionEnabled = true
            guard let self = self else { return }
            for subview in self.view.subviews {
                if subview is OverlayView {
                    subview.removeFromSuperview()
                }
            }
        }
    }
    
    fileprivate class OverlayView: UIView {
    }
}
