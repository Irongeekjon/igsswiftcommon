//
//  UItextField.swift
//  IGSSwiftCommon
//
//  Created by Jonathan on 11/7/18.
//  Copyright © 2018 IronGeekSolutions. All rights reserved.
//

import UIKit

extension UITextField {
    public func setBottomBorder(color: UIColor = .lightGray) {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor

        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
