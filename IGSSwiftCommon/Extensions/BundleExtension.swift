//
//  BundleExtension.swift
//  IGSSwiftCommon
//
//  Created by Jonathan on 11/27/18.
//  Copyright © 2018 IronGeekSolutions. All rights reserved.
//

import UIKit

extension Bundle {
    public var releaseVersionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }
    public var buildVersionNumber: String {
        return infoDictionary?["CFBundleVersion"] as? String ?? ""
    }
}
