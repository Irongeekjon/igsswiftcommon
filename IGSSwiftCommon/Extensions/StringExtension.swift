//
//  StringExtension.swift
//  IGSSwiftCommon
//
//  Created by Jonathan on 11/27/18.
//  Copyright © 2018 IronGeekSolutions. All rights reserved.
//

import Foundation

import Foundation

extension String {
    public var valueAsInteger: Int {
        return Int(self.numbersOnly) ?? 0
    }
    public var numbersOnly: String {
        return self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
    }
}
