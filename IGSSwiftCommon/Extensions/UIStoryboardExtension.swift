//
//  UIStoryboardExtension.swift
//  IGSSwiftCommon
//
//  Created by Jonathan on 11/27/18.
//  Copyright © 2018 IronGeekSolutions. All rights reserved.
//

import UIKit

extension UIStoryboardSegue {
    public func firstViewController(from: UIViewController? = nil) -> UIViewController {
        let vcToCheck = from ?? self.destination
        if let nav = vcToCheck as? UINavigationController {
            return firstViewController(from: nav.topViewController)
        }
        if let tab = vcToCheck as? UITabBarController {
            return firstViewController(from: tab.selectedViewController)
        }
        return vcToCheck
    }
}
