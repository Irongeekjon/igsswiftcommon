//
//  IntExtension.swift
//  IGSSwiftCommon
//
//  Created by Jonathan on 12/16/18.
//  Copyright © 2018 IronGeekSolutions. All rights reserved.
//

import Foundation

extension Int {
    public var valueAsString: String {
        return "\(self)"
    }
    public var valueAsDouble: Double {
        return Double(self)
    }
    public var valueAsFloat: Float {
        return Float(self)
    }
}
