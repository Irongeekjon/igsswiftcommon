//
//  UISegmentedControlExtension.swift
//  IGSSwiftCommon
//
//  Created by Jonathan on 12/16/18.
//  Copyright © 2018 IronGeekSolutions. All rights reserved.
//

import UIKit

extension UISegmentedControl {
    public func indexMatching(title: String) -> Int? {
        guard self.numberOfSegments > 0 else {
            return nil
        }
        for index in 0...self.numberOfSegments {
            if self.titleForSegment(at: index) == title {
                return index
            }
        }
        return nil
    }
}
