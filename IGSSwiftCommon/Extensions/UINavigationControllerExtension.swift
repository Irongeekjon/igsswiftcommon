//
//  UINavigationControllerExtension.swift
//  IGSSwiftCommon
//
//  Created by Jonathan on 11/27/18.
//  Copyright © 2018 IronGeekSolutions. All rights reserved.
//

import Foundation

import UIKit

public protocol BackButtonHandler {
    func shouldPopOnBackButton() -> Bool
}

extension UINavigationController: UINavigationBarDelegate  {
    public func navigationBar(_ navigationBar: UINavigationBar, shouldPop item: UINavigationItem) -> Bool {
        
        if viewControllers.count < (navigationBar.items?.count) ?? 0 {
            return true
        }
        
        var shouldPop = true
        let vc = self.topViewController
        
        if let vc = vc as? BackButtonHandler {
            shouldPop = vc.shouldPopOnBackButton()
        }
        
        if shouldPop {
            DispatchQueue.main.async {[weak self] in
                _ = self?.popViewController(animated: true)
            }
        } else {
            for subView in navigationBar.subviews {
                if(0 < subView.alpha && subView.alpha < 1) {
                    UIView.animate(withDuration: 0.25, animations: {
                        subView.alpha = 1
                    })
                }
            }
        }
        
        return false
    }
}
