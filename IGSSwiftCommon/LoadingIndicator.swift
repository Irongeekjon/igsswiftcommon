//
//  LoadingIndicator.swift
//  CustomerServices
//
//  Created by Jonathan on 9/23/18.
//  Copyright © 2018 IronGeekSolutions. All rights reserved.
//

import UIKit

public class Loader: NSObject {
    
    private override init() {}
    
    var animationVew: UIActivityIndicatorView?
    
    static let shared: Loader = {
        let progressHUD = Loader()
        return progressHUD
    }()
    
    fileprivate func createAnimationVew() -> UIActivityIndicatorView? {
        return UIActivityIndicatorView(style: .whiteLarge)
    }
    
    public class func show(inView: UIView? = nil) {

        DispatchQueue.main.async {
            
            let topView = UIApplication.shared.topViewController?.view
            guard let validView = inView ?? topView else {
                return
            }
            
            guard let animationVew = Loader.shared.createAnimationVew() else {
                return
            }
            
            validView.addSubview(animationVew)
            animationVew.translatesAutoresizingMaskIntoConstraints = false
            let aspectRatio = NSLayoutConstraint(item: animationVew,
                                                 attribute: .height,
                                                 relatedBy: .equal,
                                                 toItem: validView,
                                                 attribute: .width,
                                                 multiplier: (1.0 / 5.0),
                                                 constant: 0)
            let equalHeight = NSLayoutConstraint(item: animationVew,
                                                 attribute: .height,
                                                 relatedBy: .equal,
                                                 toItem: animationVew,
                                                 attribute: .width,
                                                 multiplier: (168.0/239.0),
                                                 constant: 0)
            let centerX =  NSLayoutConstraint(item: animationVew,
                                              attribute: NSLayoutConstraint.Attribute.centerX,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: validView,
                                              attribute: NSLayoutConstraint.Attribute.centerX,
                                              multiplier: 1,
                                              constant: 0)
            
            var centerY: NSLayoutConstraint?
            if let scrollView = validView as? UIScrollView {
                let yOffset: CGFloat = scrollView.contentOffset.y + (validView.bounds.height / 2)
                centerY = NSLayoutConstraint(item: animationVew,
                                             attribute: .top,
                                             relatedBy: .equal,
                                             toItem: scrollView,
                                             attribute: .top,
                                             multiplier: 1,
                                             constant: yOffset)
            } else {
                centerY = NSLayoutConstraint(item: animationVew,
                                             attribute: NSLayoutConstraint.Attribute.centerY,
                                             relatedBy: NSLayoutConstraint.Relation.equal,
                                             toItem: validView,
                                             attribute: NSLayoutConstraint.Attribute.centerY,
                                             multiplier: 1,
                                             constant: 0)
            }
            
            guard let validCenterY = centerY else {
                return
            }
            validView.addConstraints([aspectRatio, equalHeight, centerX, validCenterY])
            animationVew.startAnimating()
        }
    }
    
    public class func dismiss() {
        DispatchQueue.main.async {
            Loader.shared.animationVew?.stopAnimating()
            Loader.shared.animationVew?.removeFromSuperview()
        }
    }
}
