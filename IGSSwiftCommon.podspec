Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '13.0'
s.name = "IGSSwiftCommon"
s.summary = "IGSSwiftCommon contains extensions and helpers"
s.requires_arc = true

s.version = "1.0.0"

s.license = { :type => "MIT", :file => "LICENSE" }
s.author = { "Jonathan Hebert" => "jhebert@irongeeksolutions.com" }
s.homepage = "https://bitbucket.org/Irongeekjon/IGSSwiftCommon/src/master/"

s.source = { :git => "https://bitbucket.org/Irongeekjon/IGSSwiftCommon.git",
:tag => "#{s.version}" }

s.framework = "UIKit"
s.dependency 'MBProgressHUD'

s.source_files = "IGSSwiftCommon/**/*.{swift}"
# s.resources = "IGSSwiftCommon/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

s.swift_version = "4.2"

end
